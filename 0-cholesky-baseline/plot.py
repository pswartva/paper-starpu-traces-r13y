import glob
from matplotlib import pyplot as plt
import pandas as pd

fig, ax = plt.subplots()

for f in glob.glob("cholesky_*.out"):
	df = pd.read_csv(f, delim_whitespace=True)
	s = df.groupby('n').agg({"gflops": ["min", "max", "mean"]})

	plt.plot(s.index, s['gflops']['mean'], 'o-', label=f)
	plt.fill_between(s.index, s['gflops']['min'], s['gflops']['max'], alpha=0.3)

ax.grid()
ax.set(xlabel="Matrix size", ylabel="Gflops", title="Cholesky performance baseline")
ax.set_ylim(bottom=0)
ax.legend()
plt.savefig("cholesky.pdf")
