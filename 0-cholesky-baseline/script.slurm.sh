#!/bin/bash

#SBATCH --tasks-per-node=1
#SBATCH --exclusive
#SBATCH -t 15:00

if [[ $SLURM_JOB_NODELIST == zonda* ]];
then
	echo "Zonda node used, so setting MKL env vars"
	export MKL_DEBUG_CPU_TYPE=5
	export MKL_ENABLE_INSTRUCTIONS=AVX512
	export MKL_NUM_THREADS=1
fi

echo "**** Running on $SLURM_JOB_NUM_NODES node(s) ($SLURM_JOB_NODELIST) ****"

nb_workers=$((SLURM_CPUS_PER_TASK-1))
echo "$nb_workers workers"

date
guix time-machine --channels=../../guix-channels.scm -- \
	environment --pure --preserve=^SLURM --preserve=^MKL --ad-hoc chameleon slurm@19 --with-input=openmpi=nmad --with-input=openblas=mkl -- \
	mpirun hostname
date
guix time-machine --channels=../../guix-channels.scm -- \
	environment --pure --preserve=^SLURM --preserve=^MKL --ad-hoc chameleon slurm@19 --with-input=openmpi=nmad --with-input=openblas=mkl -- \
	mpirun -DSTARPU_NCPU=$nb_workers -DSTARPU_MAIN_THREAD_BIND=1 chameleon_stesting -o potrf --n 4800:40000:3200 -H --mtxfmt=1 --niter 3 \
	> cholesky_simple_${SLURM_JOB_NUM_NODES}.out
date
guix time-machine --channels=../../guix-channels.scm -- \
	environment --pure --preserve=^SLURM --preserve=^MKL --ad-hoc chameleon slurm@19 --with-input=openmpi=nmad --with-input=openblas=mkl -- \
	mpirun -DSTARPU_NCPU=$nb_workers -DSTARPU_MAIN_THREAD_BIND=1 chameleon_dtesting -o potrf --n 4800:40000:3200 -H --mtxfmt=1 --niter 3 \
	> cholesky_double_${SLURM_JOB_NUM_NODES}.out
date

if [ $SLURM_JOB_NUM_NODES = 2 ]
then
	export MPLBACKEND="agg"
	guix time-machine --channels=../../guix-channels.scm -- \
		environment --pure --preserve=MPLBACKEND -m ../../guix-manifest-plot.scm -- \
		python3 ../plot.py
	date
fi
