#!/bin/bash

mkdir -p bora
cd bora
previous_job=$(sbatch -N 1 -C bora --cpus-per-task=36 ../script.slurm.sh | cut -d " " -f 4)
previous_job=$(sbatch -N 2 -C bora --cpus-per-task=36 --dependency=afterany:$previous_job ../script.slurm.sh | cut -d " " -f 4)
cd ..

mkdir -p zonda
cd zonda
previous_job=$(sbatch -N 1 -C zonda --cpus-per-task=64 ../script.slurm.sh | cut -d " " -f 4)
previous_job=$(sbatch -N 2 -C zonda --cpus-per-task=64 --dependency=afterany:$previous_job ../script.slurm.sh | cut -d " " -f 4)
cd ..
