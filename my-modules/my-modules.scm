(define-module (my-modules)
               #:use-module (guix build-system r)
               #:use-module (guix git-download)
               #:use-module ((guix licenses) #:prefix license:)
               #:use-module (guix packages)
               #:use-module (gnu packages base)
               #:use-module (gnu packages bash)
               #:use-module (gnu packages commencement)
               #:use-module (gnu packages compression)
               #:use-module (gnu packages cran)
               #:use-module (gnu packages databases)
               #:use-module (gnu packages gawk)
               #:use-module (gnu packages python)
               #:use-module (gnu packages statistics)
               #:use-module (guix utils)
               #:use-module (srfi srfi-1)
               #:use-module (inria eztrace)
               #:use-module (inria storm)
               #:use-module (inria tadaam)
               #:use-module (ufrgs ufrgs))

(define-public my-starpu-fxt
  (package
    (inherit starpu)
    (name "my-starpu-fxt")
    (inputs `(("fxt" ,fxt)
              ,@(package-inputs starpu)))
    (native-inputs
     `(("python-wrapper" ,python-wrapper)
       ,@(package-native-inputs starpu)))))

(define-public my-starpu-fxt-mpi-sync-clocks
  (package
    (inherit starpu)
    (name "my-starpu-fxt-mpi-sync-clocks")
    (inputs `(("fxt" ,fxt)
              ("mpi_sync_clocks" ,mpi_sync_clocks)
              ,@(package-inputs starpu)))
    (native-inputs
     `(("python-wrapper" ,python-wrapper)
       ,@(package-native-inputs starpu)))))

(define-public my-r-starvz
  (package
   (name "my-r-starvz")
   (version "0.5.1")
   (home-page "https://github.com/schnorr/starvz")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url home-page)
           (commit "b789296a90e22ae8cd73a6a58df3ff2bd1ff02e3")
           (recursive? #f)))
     (file-name (string-append name "-" version "-checkout"))
     (sha256
      (base32
       "0idms0b61nx8djhpapa0f3015ff3yjz6xmbikjpxdsz0xvrjx4q3"))))
   (properties
    `((upstream-name . "starvz")))
   (build-system r-build-system)
   (propagated-inputs
    `(("awk" ,gawk)
      ("bash" ,bash)
      ("coreutils" ,coreutils)
      ("gcc-toolchain" ,gcc-toolchain)
      ("grep" ,grep)
      ("gzip" ,gzip)
      ("pageng" ,pageng)
      ("r-arrow" ,r-arrow-cpp)
      ("r-bh" ,r-bh)
      ("r-car" ,r-car)
      ("r-data-tree" ,r-data-tree)
      ("r-dplyr" ,r-dplyr)
      ("r-flexmix" ,r-flexmix)
      ("r-ggplot2" ,r-ggplot2)
      ("r-gtools" ,r-gtools)
      ("r-lpsolve" ,r-lpsolve)
      ("r-magrittr" ,r-magrittr)
      ("r-patchwork" ,r-patchwork)
      ("r-purrr" ,r-purrr)
      ("r-rcolorbrewer" ,r-rcolorbrewer)
      ("r-rcpp" ,r-rcpp)
      ("r-readr" ,r-readr)
      ("r-rlang" ,r-rlang)
      ("r-stringr" ,r-stringr)
      ("r-tibble" ,r-tibble)
      ("r-tidyr" ,r-tidyr)
      ("r-yaml" ,r-yaml)
      ("r-zoo" ,r-zoo)
      ("rectutils" ,recutils)
      ("sed" ,sed)
      ("starpu" ,my-starpu-fxt)
      ("which" ,which)))
   (synopsis
    "R-Based Visualization Techniques for Task-Based Applications")
   (description
    "Performance analysis workflow that combines the power of the R
language (and the tidyverse realm) and many auxiliary tools to provide a
consistent, flexible, extensible, fast, and versatile framework for the
performance analysis of task-based applications that run on top of the StarPU
runtime (with its MPI (Message Passing Interface) layer for multi-node support).
Its goal is to provide a fruitful prototypical environment to conduct
performance analysis hypothesis-checking for task-based applications that run on
heterogeneous (multi-GPU, multi-core) multi-node HPC (High-performance
computing) platforms.")
   (license license:gpl3)))
