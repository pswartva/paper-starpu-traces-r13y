#!/bin/bash


BEEGFS_PATH=/tmp
TRACE_NAME=prof_file_pswartva_0
KERNEL_CODE=lacpy

for nworkers in $(seq 2 2 51)
do
	echo "***** $nworkers workers ******"
	date

	# Without tracing:
	guix time-machine --channels=../guix-channels.scm -- \
		environment --pure --ad-hoc chameleon \
		--without-tests=starpu --with-input=openmpi=nmad --with-input=openblas=mkl --with-patch=chameleon=chameleon-flops.diff -- \
		mpirun -DSTARPU_NCPU=$nworkers -DSTARPU_MAIN_THREAD_BIND=1 \
		chameleon_stesting -o $KERNEL_CODE --n 35200 -b 320 -H --mtxfmt=1 --niter 10 --nowarmup --alpha 3.14 \
		> ${KERNEL_CODE}_${nworkers}.out

	date

	# With tracing:
	guix time-machine --channels=../guix-channels.scm -- \
		environment --pure --ad-hoc chameleon \
		--with-input=starpu=starpu-fxt --without-tests=starpu-fxt --with-input=openmpi=nmad --with-input=openblas=mkl --with-patch=chameleon=chameleon-flops.diff -- \
		mpirun -DSTARPU_NCPU=$nworkers -DSTARPU_TRACE_BUFFER_SIZE=4096 -DSTARPU_FXT_PREFIX=$BEEGFS_PATH/ -DSTARPU_MAIN_THREAD_BIND=1 \
		chameleon_stesting -o ${KERNEL_CODE} --n 35200 -b 320 -H --mtxfmt=1 --niter 10 --trace --nowarmup --alpha 3.14 \
		> ${KERNEL_CODE}_trace_${nworkers}.out
	ls -lh $BEEGFS_PATH/$TRACE_NAME
	rm $BEEGFS_PATH/$TRACE_NAME
done

date

export MPLBACKEND="agg"
guix time-machine --channels=../guix-channels.scm -- \
	environment --pure --preserve=MPLBACKEND -m ../guix-manifest-plot.scm -- \
	python3 plot.py ${KERNEL_CODE} "Matrix copy"
