from matplotlib import pyplot as plt
import numpy as np


first_event = 407574309
last_event = 942518788
local_first_barrier = 557764530
offset_start = -376804908
local_end_barrier = 633623850
offset_end = -316899298

def interpolate_offset(x, restrict=False):
    yB = offset_end
    yA = offset_start
    xB = local_end_barrier
    xA = local_first_barrier

    if restrict:
        if x < xA:
            return offset_start
        elif x > xB:
            return offset_end

    offset = ((yB-yA) / (xB-xA)) * (x-xA) + yA

    return offset


ax1 = plt.subplot(211)

plt.plot([local_first_barrier, local_end_barrier], [-offset_start, -offset_end], 'o--')
plt.axvline(x=first_event, color="black")
plt.axvline(x=last_event, color="black")

plt.ylabel("Offset to apply")
plt.title("Offset to apply")
plt.setp(ax1.get_xticklabels(), visible=False)



ax2 = plt.subplot(212, sharex=ax1)

plt.axvline(x=first_event, color="black")
plt.axvline(x=last_event, color="black")

x = np.linspace(first_event, last_event)

plt.plot(x, x, label="Without correction")
plt.plot(x, [i+interpolate_offset(i) for i in x], label="With correction")
plt.plot(x, [i+interpolate_offset(i, restrict=True) for i in x], label="With correction only in\nsynchronized window")

y_lim_bottom, _ = ax2.get_ylim()
ax2.legend(prop={'size': 9})
ax2.axhspan(0, y_lim_bottom-1, facecolor="0.9", zorder=0.1)
ax2.set_ylim(bottom=y_lim_bottom)
plt.xlabel("Local time")
plt.ylabel("Corrected time (= in global time)")
plt.title("Time correction")

plt.savefig("clock_offset_example.pdf")
