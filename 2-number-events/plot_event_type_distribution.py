import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv(
        "number_events.out",
        engine='python',
        delim_whitespace=True,
        skipinitialspace=True,
        names=['nb', 'name'],
        skipfooter=1
)

df['name'] = df['name'].apply(lambda s: s.replace("_STARPU_", "").replace("FUT_", ""))
to_plot = df[df['nb']>2000].sort_values(by=['name'])

plt.rc('xtick', labelsize=8)
plt.gca().set(title="Repartition of the number of events", ylabel="Number of events")
plt.gca().set_axisbelow(True)
plt.bar(to_plot['name'], to_plot['nb'])
plt.xticks(rotation=45, ha="right")
plt.subplots_adjust(bottom=0.35)
plt.gcf().set_size_inches(plt.rcParams["figure.figsize"][0] * 2, plt.rcParams["figure.figsize"][1])
plt.grid(axis='y')
plt.savefig("type_events_distribution.pdf")
