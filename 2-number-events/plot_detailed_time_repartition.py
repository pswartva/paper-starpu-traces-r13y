from matplotlib import pyplot as plt
from matplotlib import colors
import numpy as np
import os
import pandas as pd
import pickle

USE_PICKLE = False


def stack_hist(ax, stacked_data, nbins, labels):
        min_value = stacked_data[0].values[0]
        max_value = min_value

        for series in stacked_data:
                for v in series:
                        if v < min_value:
                                min_value = v
                        elif v > max_value:
                                max_value = v

        bins = np.linspace(min_value, max_value, nbins, endpoint=True)
        data_colors = [colors.hsv_to_rgb([(i * 0.618033988749895) % 1.0, 0.7, 1]) for i in range(len(stacked_data))]
        hatches = ['/', '\\', '|', '-', '+', 'x', 'o', 'O', '.', '*']
        bottoms = np.zeros(nbins-1)

        for j, (data, label) in enumerate(zip(stacked_data, labels)):
                vals, edges = np.histogram(data, bins=bins)
                top = bottoms + vals
                # alpha=1 because of a bug of hatch rendering in PDF exports (see https://github.com/matplotlib/matplotlib/issues/12367)
                ax.fill_between(edges, np.append(top, top[-1]), np.append(bottoms, bottoms[-1]), step='post', label=label, facecolor=data_colors[j], hatch=hatches[j % len(hatches)], alpha=1)
                bottoms = top



# First, get names corresponding to event codes.
# This file is generated with the following command:
# grep -E "#define\s+_STARPU_(MPI_)?FUT_" src/common/fxt.h mpi/src/starpu_mpi_fxt.h | grep 0x | grep -v 0x1 | cut -d : -f 2 > event_codes.out
code_names = dict()
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "event_codes.out"), 'r') as f:
        for line in f:
                elements = line.split()
                if len(elements) == 3:
                        key = elements[2][2:]
                        assert key not in code_names
                        code_names[key] = elements[1].replace("_STARPU_", "").replace("FUT_", "")

def code2name(code):
        if code in code_names:
                return code_names[code]
        else:
                return code


def get_data():
        pickle_filename = "detailed_time_repartition.pickle"
        if USE_PICKLE and os.path.exists(pickle_filename):
                with open(pickle_filename, "rb") as f:
                        return pickle.load(f)
        else:
                df = pd.read_csv("trace.raw", sep="\t", skiprows=10, usecols=[0, 3], names=['time', 'code'])
                df['time'] = df['time'].apply(lambda s: int(s[:-1]) / 1e6)  # remove the ending 'u' and convert to int and scale to milliseconds
                df['code'] = df['code'].apply(lambda s: code2name(s[:s.find('(')].strip()))

                codes = df.groupby('code').agg({'code': ['size']})
                kept_codes = codes[codes['code']['size'] > 60000]['code'].index

                code_times = []
                for c in kept_codes:
                        code_times.append(df[df['code']==c]['time'])

                if USE_PICKLE:
                        with open(pickle_filename, "wb") as f:
                                pickle.dump((code_times, kept_codes), f)

                return code_times, kept_codes


code_times, kept_codes = get_data()

fig, ax = plt.subplots()
stack_hist(ax, code_times, 100, kept_codes)
ax.set(title="Number and type of recorded events across execution time", ylabel="Number of events", xlabel="Time (ms)")

# Put the legend items in the same order of the stacked series:
legend_handles, legend_labels = ax.get_legend_handles_labels()
plt.legend(legend_handles[::-1], legend_labels[::-1], loc="center left", bbox_to_anchor=(1, 0.5), fontsize=8)

plt.subplots_adjust(right=0.85)
fig.set_size_inches(15, 9)
plt.savefig("detailed_number_events_time.pdf")
# plt.show()
