#!/bin/bash

guix time-machine --channels=../../guix-channels.scm -- \
	environment --pure -m ../../guix-manifest-plot.scm -- \
	python3 ../plot_event_type_distribution.py

guix time-machine --channels=../../guix-channels.scm -- \
	environment --pure -m ../../guix-manifest-plot.scm -- \
	python3 ../plot_time_repartition.py

guix time-machine --channels=../../guix-channels.scm -- \
	environment --pure -m ../../guix-manifest-plot.scm -- \
	python3 ../plot_detailed_time_repartition.py
