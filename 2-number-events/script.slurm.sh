#!/bin/bash

#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=36
#SBATCH --exclusive
#SBATCH -C bora
#SBATCH -N 1
#SBATCH -t 15:00

BEEGFS_PATH=/beegfs/pswartva/traces
TRACE_NAME=prof_file_pswartva_0

function run()
{
	echo "** Running $1 $2 version **"
	mkdir -p $1
	cd $1;
	date
	# * We precise a commit from the master branch of StarPU, since the
	#   -number-events option isn't released yet.
	# * We use the parent of commit cfb65fee9917b2e96277190b3285c523d811a3bb, since
	#   this commit introduce automatic selection of the number of CPU cores, which
	#   is not well supported by Guix, for now.
	# * We apply a patch to Chameleon, since waiting for the whole task graph to be
	#   submitted before executing tasks is not in the master branch yet.
	# * The second time, we wait for the whole graph to be submitted before executing tasks, to see
	#   which event types are triggered during these two different phases.
	guix time-machine --channels=../../guix-channels-working-with-patch.scm -- \
		environment --pure --ad-hoc chameleon slurm@19 \
		--with-input=starpu=starpu-fxt --with-commit=starpu-fxt=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-patch=chameleon=../chameleon-wait-graph-submission.patch --with-input=openmpi=nmad --with-input=openblas=mkl -- \
		mpirun -DSTARPU_NCPU=35 -DSTARPU_TRACE_BUFFER_SIZE=256 -DSTARPU_FXT_TRACE=1 -DSTARPU_FXT_PREFIX=$BEEGFS_PATH/ -DSTARPU_MAIN_THREAD_BIND=1 \
		$2 -o potrf --n 24000 -H --mtxfmt=1 --niter 1 --trace --nowarmup $3 \
		&> cholesky.out

	date

	echo "* Calling starpu_fxt_tool *"
	guix time-machine --channels=../../guix-channels.scm -- \
		environment --pure --ad-hoc chameleon slurm@19 --with-input=starpu=starpu-fxt --with-commit=starpu-fxt=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-input=openmpi=nmad --with-input=openblas=mkl -- \
		starpu_fxt_tool -number-events -i $BEEGFS_PATH/$TRACE_NAME

	echo "* Calling starpu_fxt_number_events_to_names *"
	guix time-machine --channels=../../guix-channels.scm -- \
		environment --pure --ad-hoc starpu-fxt --with-commit=starpu-fxt=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-input=openmpi=nmad --with-input=openblas=mkl -- \
		starpu_fxt_number_events_to_names.py number_events.data > number_events.out

	echo "* Calling fxt_print *"
	guix time-machine --channels=../../guix-channels.scm -- \
		environment --pure --ad-hoc fxt -- \
		fxt_print -o -f $BEEGFS_PATH/$TRACE_NAME > trace.raw

	ls -lh $BEEGFS_PATH/$TRACE_NAME
	mv $BEEGFS_PATH/$TRACE_NAME ./$TRACE_NAME
	rm -f activity.data comms.rec dag.dot data.rec number_events.data distrib.data sched_tasks.rec tasks.rec trace.html trace.rec

	echo "* Plotting *"
	../plot.sh

	cd ..

	date
}

run simple_not_paused chameleon_stesting
run simple_paused chameleon_stesting -S

run double_not_paused chameleon_dtesting
run double_paused chameleon_dtesting -S
