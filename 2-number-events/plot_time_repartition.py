import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv("trace.raw", sep="\t", skiprows=10, usecols=[0], names=['time'])
df['time'] = df['time'].apply(lambda s: int(s[:-1]))  # remove the ending 'u' and convert to int
df['time'] /= 1e6  # scale to milliseconds

df['time'].hist(bins=50, histtype='step')
plt.gca().set(title="Number of recorded events across execution time", ylabel="Number of events", xlabel="Time (ms)")
plt.gca().set_axisbelow(True)
plt.subplots_adjust(left=0.15)
plt.savefig("number_events_time.pdf")
