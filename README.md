# Paper StarPU Traces Reproducibility



[![SWH](https://archive.softwareheritage.org/badge/origin/https://gitlab.inria.fr/pswartva/paper-starpu-traces-r13y/)](https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.inria.fr/pswartva/paper-starpu-traces-r13y)
[![SWH](https://archive.softwareheritage.org/badge/swh:1:dir:52b49a8bc10d91fc5ebc2336cf56915ee25de7ee/)](https://archive.softwareheritage.org/swh:1:dir:52b49a8bc10d91fc5ebc2336cf56915ee25de7ee;origin=https://gitlab.inria.fr/pswartva/paper-starpu-traces-r13y;visit=swh:1:snp:e098c2012fa26baf67056e82f8d822dfcecc08cb;anchor=swh:1:rev:bdab3c356c5402659b66381f02d8765322c4b7d1)

This repository contains the material to reproduce the experiments presented in
the paper *Tracing task-based runtime systems: feedbacks from the StarPU case*.


## Requirements

Regarding the hardware, you need one HPC node to reproduce experiments made on
a single node (section 4 *Reducing Impact on Performances*), and then two
networked HPC nodes for the experiments about the distributed clocks (section 5
*Precise Distributed Traces*).

The scripts use SLURM as job scheduler, but they can be adapted to other job
schedulers (or also to the case without a job scheduler).

To ensure easy reproducibility of the experiments, we use the [Guix package
manager](http://guix.gnu.org/), with the
[Guix-HPC](https://gitlab.inria.fr/guix-hpc/guix-hpc/) and [Guix-HPC non
free](https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free) channels.

If you cannot use Guix, the following table lists the software blocks used for
the experiments. For some experiments, we use a specific commit of the
libraries and/or apply patches. See the `script.slurm.sh` scripts for the
details.

| Package      | Version    | Website                                       | Code location                                                                             | Commit                                   |
|---------     |---------   |---------                                      |---------------                                                                            |--------                                  |
| NewMadeleine | 2021-05-21 | https://pm2.gitlabpages.inria.fr/NewMadeleine | https://gitlab.inria.fr/pm2/pm2.git                                                       | 2765619bd2c77af733778643f369ba086a29715a |
| StarPU       | 1.3.8      | http://starpu.gforge.inria.fr                 | https://gitlab.inria.fr/starpu/starpu.git                                                 | starpu-1.3.8                             |
| Chameleon    | 1.1.0      | https://gitlab.inria.fr/solverstack/chameleon | https://gitlab.inria.fr/solverstack/chameleon                                             | 4db899ca30d29927018d83964b9b6d517269abe1 |
| StarVZ       | 0.5.1      | https://github.com/schnorr/starvz             | https://github.com/schnorr/starvz                                                         | b789296a90e22ae8cd73a6a58df3ff2bd1ff02e3 |
| GCC          | 11.2.0     | https://gcc.gnu.org/                          |                                                                                           |                                          |
| Slurm        | 19.05.8    | https://slurm.schedmd.com/                    | https://download.schedmd.com/slurm/slurm-19.05.8.tar.bz2                                  |                                          |
| Intel MKL    | 2019.1.144 | https://software.intel.com/en-us/mkl          | http://registrationcenter-download.intel.com/akdlm/irc_nas/tec/14895/l_mkl_2019.1.144.tgz |                                          |
| FxT          | 0.3.14     | https://savannah.nongnu.org/projects/fkt      | http://download.savannah.nongnu.org/releases/fkt/fxt-0.3.14.tar.gz                        |                                          |
| Python       | 3.8.2      | https://www.python.org                        | https://www.python.org/ftp/python/3.8.2/Python-3.8.2.tar.xz                               |                                          |
| Matplotlib   | 3.1.2      | https://matplotlib.org/                       | https://files.pythonhosted.org/packages/source/m/matplotlib/matplotlib-3.1.2.tar.gz       |                                          |
| Pandas       | 1.3.0      | https://pandas.pydata.org                     | https://files.pythonhosted.org/packages/source/p/pandas/pandas-1.3.0.tar.gz               |                                          |



## Organisation

Each folder contains the material to reproduce a given experiments (setting the
environment, scripts to launch the experiments and to plot the results). The
scripts `script.sh` are the ones to be launched by the end-user to launch the
experiments. They actually launch SLURM jobs, defined in the files named
`script.slurm.sh`. The Python scripts are used to analyze the execution outputs
and generate plots. They are sometimes wrapped in a `plot.sh` script to setup
the Guix environment, or directely called from `script.sh` or `script.slurm.sh`.



## Experiments

This section describes in details each experiments. The order to execute them
is for some of them important (some of them use the results of previous
experiments).

We use custom package definitions (located in the folder `my-modules`) to avoid
a bug in Guix when combining StarPU, FxT and NewMadeleine. We often precise the
commit to use for StarPU to have recent features not yet packaged in Guix (at
the time of the experiments) and to avoid a commit breaking the compilation
with Guix.

You need to change the `BEEGFS_PATH` variable in many scripts, to make it point
to a folder when traces can be written. This requires lot of space (O(100 GB)).

To visualize Pajé traces, you need
[ViTE](https://solverstack.gitlabpages.inria.fr/vite/). With Guix:
```bash
guix time-machine --channels=../guix-channels.scm -- \
    environment --pure --ad-hoc vite -- \
    vite paje.trace
```



### 0 - Cholesky Baseline

The first thing to do is to evaluate the Cholesky performance on the given
cluster.

The script `script.sh` launches jobs to evaluate the Cholesky performances of 1
and 2 nodes of two different types of nodes: `bora` and `zonda`. Then, each job
executes the benchmark in simple and double precision and plot the results.



### 1 - Impact of buffer flushes

This experiment evaluates the impact of event trace buffer flushes in the
middle of appliction execution (section 4.1 of the paper).

You may need to change:
- the size of the matrix to work on (24000 in our case); the chosen value is
  the beginning of the performance peak obtained with the *Cholesky Baseline*
  benchmark.
- the number of iterations of the Cholesky decompositions; there have to be
  enough of them to produce several buffer flushes during the execution.
- the size of the trace buffer (`-DSTARPU_TRACE_BUFFER_SIZE`): it has to be
  large enough to contain the events of several Cholesky iterations, but small
  enough to produce some of buffer flushes (and fit in your memory !).

At the end of the job, the script plotting the figures 2 and 4 of the paper
will be executed.



### 2 - Counting the number of events

This experiment counts the number of events recorded for single Cholesky
decomposition in different configurations: simple or double precesion, paused
workers or not during the graph submission (figures 5, 6 and 7 of the section
4.2 of the paper).



### 3 - Reducing the number of events

`script.sh` submits several jobs on 1 and 2 nodes to see the impact of reducing
the number of events to record.

The script `plot_all.sh` can be used to generate all the plot without executing
the experiments just before (useful when working on the plot generation).



### 4 - Impact of the number of recording cores

This experiment compare the difference of performances with and without tracing
enabled, with different number of cores recording events (figures 10, 11 and 12
of the section 4.3 of the paper).


### 4 - Impact of the number of recording cores on a memory-bound application

Same as the previous experiment, but with a matrix copy as memory-bound kernel
(figure 13 of the section 4.3 of the paper).


### 5 - Different clock synchronization methods

`script.sh` builds an application executing several times communications
following a ring pattern, then traces its execution with different
synchronization methods (figures 15 and 17 in the section 5.3.1 of the paper).



### 6 - Adjusting timestamps only in the synchronized window

This folder contains the patch used to debug the problems of wrong timestamps
outside of the synchronized window. The Python script plots the figure 19 of
the section 5.3.4 of the paper, with the values obtained during debbuging.


### 7 - Visualization with StarVZ

The script `script.sh` takes the trace generated by the experiment *2 -
Counting the number of events* and generates the StarVZ visualization (figure 1
in the article).

This execution requires lot of RAM memory (> 8 GB).
