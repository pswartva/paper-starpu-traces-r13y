#!/bin/bash

#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=36
#SBATCH --exclusive
#SBATCH -C bora
#SBATCH -N 1
#SBATCH -t 10:00

BEEGFS_PATH=/beegfs/pswartva/traces
TRACE_NAME=prof_file_pswartva_0

date
# Impact on an efficient parallel filesystem (beegfs):
guix time-machine --channels=../guix-channels.scm -- \
	environment --pure --ad-hoc chameleon slurm@19 --with-input=starpu=starpu-fxt --with-input=openmpi=nmad --with-input=openblas=mkl -- \
	mpirun -DSTARPU_NCPU=35 -DSTARPU_TRACE_BUFFER_SIZE=1024 -DSTARPU_FXT_PREFIX=$BEEGFS_PATH/ -DSTARPU_MAIN_THREAD_BIND=1 \
	chameleon_stesting -o potrf --n 24000 -H --mtxfmt=1 --niter 43 --trace --nowarmup \
	&> cholesky_pfs.out

date
ls -lh $BEEGFS_PATH/$TRACE_NAME
rm -f $BEEGFS_PATH/$TRACE_NAME

# Impact on the /tmp folder of the node (HDD 7200 rpm):
guix time-machine --channels=../guix-channels.scm -- \
	environment --pure --ad-hoc chameleon slurm@19 --with-input=starpu=starpu-fxt --with-input=openmpi=nmad --with-input=openblas=mkl -- \
	mpirun -DSTARPU_NCPU=35 -DSTARPU_TRACE_BUFFER_SIZE=1024 -DSTARPU_MAIN_THREAD_BIND=1 \
	chameleon_stesting -o potrf --n 24000 -H --mtxfmt=1 --niter 43 --trace --nowarmup \
	&> cholesky_tmp.out

date

ssh $SLURM_NODELIST "ls -lh /tmp/$TRACE_NAME"
# Clean:
ssh $SLURM_NODELIST "rm /tmp/$TRACE_NAME"

date

# Make a smaller trace to inspect it with ViTE:
# You may need to execute it several times to find an execution where the buffer flush really hurts performances
guix time-machine --channels=../guix-channels.scm -- \
	environment --pure --ad-hoc chameleon slurm@19 --with-input=starpu=starpu-fxt --with-input=openmpi=nmad --with-input=openblas=mkl -- \
	mpirun -DSTARPU_NCPU=35 -DSTARPU_TRACE_BUFFER_SIZE=512 -DSTARPU_GENERATE_TRACE=1 -DSTARPU_FXT_PREFIX=$BEEGFS_PATH/ -DSTARPU_MAIN_THREAD_BIND=1 \
	chameleon_stesting -o potrf --n 24000 -H --mtxfmt=1 --niter 10 --trace --nowarmup \
	&> cholesky_trace.out

date

# Clean:
rm -f {activity.data,dag.dot,data.rec,distrib.data,tasks.rec,trace.html,trace.rec}

date
# Now with a buffer big enough to not flush during execution:
guix time-machine --channels=../guix-channels.scm -- \
	environment --pure --ad-hoc chameleon slurm@19 --with-input=starpu=starpu-fxt --with-input=openmpi=nmad --with-input=openblas=mkl -- \
	mpirun -DSTARPU_NCPU=35 -DSTARPU_TRACE_BUFFER_SIZE=8192 -DSTARPU_FXT_PREFIX=$BEEGFS_PATH/ -DSTARPU_MAIN_THREAD_BIND=1 \
	chameleon_stesting -o potrf --n 24000 -H --mtxfmt=1 --niter 43 --trace --nowarmup \
	&> cholesky_pfs_big.out

date
ls -lh $BEEGFS_PATH/$TRACE_NAME
rm -f $BEEGFS_PATH/$TRACE_NAME


./plot.sh
