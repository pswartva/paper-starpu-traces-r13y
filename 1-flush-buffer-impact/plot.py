from matplotlib import lines, pyplot as plt
import pandas as pd
import sys

df = pd.read_csv(sys.argv[1], skiprows=1, delim_whitespace=True)
df = df[~(df['Function']=="Maybe")]

cholesky = df[~(df['Function']=="FxT")]
fxt = df[df['Function']=="FxT"]

fxt_index = [ix-2*i for i, ix in enumerate(fxt.index)]

fig, ax = plt.subplots()
for value in fxt_index:
    plt.axvline(x=value, color="r")


cholesky_dots = ax.plot(cholesky['Id'], cholesky['gflops'], 'o')
ax.grid()
ax.set(xlabel="Run", ylabel="Gflops", title="Cholesky performance with traces")
ax.set_ylim(bottom=0, top=cholesky['gflops'].max()*1.1)

if len(fxt_index) > 0:
    vertical_line = lines.Line2D([], [], color='r', marker='|', linestyle='None', markersize=10, markeredgewidth=1.5)
    plt.legend([vertical_line]+cholesky_dots, ["Trace flush", "Cholesky"])
else:
    plt.legend(cholesky_dots, ["Cholesky"])

ax.xaxis.set_ticks(cholesky['Id'][::5])
plt.savefig(sys.argv[1].replace(".out", ".pdf"))
