#!/bin/bash

for f in pfs tmp trace pfs_big;
do
	echo "cholesky_"$f;

	guix time-machine --channels=../guix-channels.scm -- \
		environment --pure -m ../guix-manifest-plot.scm -- \
		python3 plot.py cholesky_${f}.out
done
