#!/bin/bash

#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=36
#SBATCH --exclusive
#SBATCH -C bora


echo "**** Executing $1 on $SLURM_JOB_NUM_NODES node(s) ($SLURM_JOB_NODELIST), with matrix size $2, size of trace buffer $3 MB ****"

BINARY=$1
BEEGFS_PATH=/beegfs/pswartva/traces/$SLURM_JOB_ID
TRACE_NAME=prof_file_pswartva_
MATRIX_SIZE=$2
FXT_BUFFER_SIZE=$3
NB_ITER=10 # 5
DATA_PATH=$BINARY/$MATRIX_SIZE

mkdir -p $DATA_PATH
mkdir -p $BEEGFS_PATH/$DATA_PATH

# We precise a commit from the master branch of StarPU, since the
# -number-events option isn't released yet.

# We use the parent of commit cfb65fee9917b2e96277190b3285c523d811a3bb, since
# this commit introduce automatic selection of the number of CPU cores, which
# is not well supported by Guix, for now.

echo "*** First, performances without tracing support ***"
date
guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure --preserve=^SLURM --ad-hoc chameleon slurm@19 \
	--with-commit=starpu=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-input=openmpi=nmad --with-input=openblas=mkl --with-patch=starpu=./starpu_fxt_events.patch -- \
	mpirun -DSTARPU_NCPU=35 -DSTARPU_MAIN_THREAD_BIND=1 \
	$BINARY -o potrf --n $MATRIX_SIZE -H --mtxfmt=1 --niter $NB_ITER --nowarmup \
	> $DATA_PATH/cholesky_${SLURM_JOB_NUM_NODES}_no_trace.out
date

echo "*** Performances with disabled tracing: ***"
guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure --preserve=^SLURM -L ../my-modules/ --ad-hoc chameleon slurm@19 \
	--with-input=starpu=my-starpu-fxt --with-commit=my-starpu-fxt=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-input=openmpi=nmad --with-input=openblas=mkl --with-patch=my-starpu-fxt=./starpu_fxt_events.patch -- \
	mpirun -DSTARPU_NCPU=35 -DSTARPU_FXT_TRACE=0 -DSTARPU_MAIN_THREAD_BIND=1 \
	$BINARY -o potrf --n $MATRIX_SIZE -H --mtxfmt=1 --niter $NB_ITER --nowarmup \
	> $DATA_PATH/cholesky_${SLURM_JOB_NUM_NODES}_disabled_trace.out

date

echo "*** Performances with all traces: ***"
guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure --preserve=^SLURM -L ../my-modules/ --ad-hoc chameleon slurm@19 \
	--with-input=starpu=my-starpu-fxt --with-commit=my-starpu-fxt=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-input=openmpi=nmad --with-input=openblas=mkl --with-patch=my-starpu-fxt=./starpu_fxt_events.patch -- \
	mpirun -DSTARPU_NCPU=35 -DSTARPU_TRACE_BUFFER_SIZE=$FXT_BUFFER_SIZE -DSTARPU_FXT_TRACE=1 -DSTARPU_FXT_PREFIX=$BEEGFS_PATH/ -DSTARPU_MAIN_THREAD_BIND=1 \
	$BINARY -o potrf --n $MATRIX_SIZE -H --mtxfmt=1 --niter $NB_ITER --trace --nowarmup \
	> $DATA_PATH/cholesky_${SLURM_JOB_NUM_NODES}_all_traces.out

date

for i in $(seq 0 $((SLURM_JOB_NUM_NODES-1)))
do
	guix time-machine --channels=../guix-channels.scm -- \
		environment --pure --ad-hoc fxt coreutils -- \
		fxt_print -d -f $BEEGFS_PATH/${TRACE_NAME}$i | wc -l >> $DATA_PATH/cholesky_${SLURM_JOB_NUM_NODES}_all_traces_number_events.out
done

for i in $(seq 0 $((SLURM_JOB_NUM_NODES-1)))
do
	mv $BEEGFS_PATH/${TRACE_NAME}$i $BEEGFS_PATH/$DATA_PATH/${SLURM_JOB_NUM_NODES}_all_traces_prof_file_$i
done

echo "*** Performances without verbose categories: ***"
guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure --preserve=^SLURM -L ../my-modules/ --ad-hoc chameleon slurm@19 \
	--with-input=starpu=my-starpu-fxt --with-commit=my-starpu-fxt=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-input=openmpi=nmad --with-input=openblas=mkl --with-patch=my-starpu-fxt=./starpu_fxt_events.patch -- \
	mpirun -DSTARPU_NCPU=35 -DSTARPU_TRACE_BUFFER_SIZE=$FXT_BUFFER_SIZE -DSTARPU_FXT_TRACE=1 -DSTARPU_FXT_PREFIX=$BEEGFS_PATH/ -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_FXT_EVENTS="user|task|data|worker|dsm|sched|lock|event|mpi|hyp" \
	$BINARY -o potrf --n $MATRIX_SIZE -H --mtxfmt=1 --niter $NB_ITER --trace --nowarmup \
	> $DATA_PATH/cholesky_${SLURM_JOB_NUM_NODES}_without_verbose.out

date

for i in $(seq 0 $((SLURM_JOB_NUM_NODES-1)))
do
	guix time-machine --channels=../guix-channels.scm -- \
		environment --pure --ad-hoc fxt coreutils -- \
		fxt_print -d -f $BEEGFS_PATH/${TRACE_NAME}$i | wc -l >> $DATA_PATH/cholesky_${SLURM_JOB_NUM_NODES}_without_verbose_number_events.out
done

for i in $(seq 0 $((SLURM_JOB_NUM_NODES-1)))
do
	mv $BEEGFS_PATH/${TRACE_NAME}$i $BEEGFS_PATH/$DATA_PATH/${SLURM_JOB_NUM_NODES}_without_verbose_prof_file_$i
done


echo "*** Performances without task deps: ***"
guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure --preserve=^SLURM -L ../my-modules/ --ad-hoc chameleon slurm@19 \
	--with-input=starpu=my-starpu-fxt --with-commit=my-starpu-fxt=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-patch=my-starpu-fxt=./starpu_fxt_events.patch --with-patch=my-starpu-fxt=./no_task_deps.patch --with-input=openmpi=nmad --with-input=openblas=mkl -- \
	mpirun -DSTARPU_NCPU=35 -DSTARPU_TRACE_BUFFER_SIZE=$FXT_BUFFER_SIZE -DSTARPU_FXT_TRACE=1 -DSTARPU_FXT_PREFIX=$BEEGFS_PATH/ -DSTARPU_MAIN_THREAD_BIND=1 \
	$BINARY -o potrf --n $MATRIX_SIZE -H --mtxfmt=1 --niter $NB_ITER --trace --nowarmup \
	> $DATA_PATH/cholesky_${SLURM_JOB_NUM_NODES}_no_task_deps.out

date

for i in $(seq 0 $((SLURM_JOB_NUM_NODES-1)))
do
	guix time-machine --channels=../guix-channels.scm -- \
		environment --pure --ad-hoc fxt coreutils -- \
		fxt_print -d -f $BEEGFS_PATH/${TRACE_NAME}$i | wc -l >> $DATA_PATH/cholesky_${SLURM_JOB_NUM_NODES}_no_task_deps_number_events.out
done

date


for i in $(seq 0 $((SLURM_JOB_NUM_NODES-1)))
do
	mv $BEEGFS_PATH/${TRACE_NAME}$i $BEEGFS_PATH/$DATA_PATH/${SLURM_JOB_NUM_NODES}_no_task_deps_prof_file_$i
done


echo "*** Performances without task deps no_task_deps_codelet_data_progress_tid.patch: ***"
guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure --preserve=^SLURM -L ../my-modules/ --ad-hoc chameleon slurm@19 \
	--with-input=starpu=my-starpu-fxt --with-commit=my-starpu-fxt=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-patch=my-starpu-fxt=./starpu_fxt_events.patch --with-patch=my-starpu-fxt=./no_task_deps_codelet_data_progress_tid.patch --with-input=openmpi=nmad --with-input=openblas=mkl -- \
	mpirun -DSTARPU_NCPU=35 -DSTARPU_TRACE_BUFFER_SIZE=$FXT_BUFFER_SIZE -DSTARPU_FXT_TRACE=1 -DSTARPU_FXT_PREFIX=$BEEGFS_PATH/ -DSTARPU_MAIN_THREAD_BIND=1 \
	$BINARY -o potrf --n $MATRIX_SIZE -H --mtxfmt=1 --niter $NB_ITER --trace --nowarmup \
	> $DATA_PATH/cholesky_${SLURM_JOB_NUM_NODES}_no_task_deps_codelet_progress.out

date

for i in $(seq 0 $((SLURM_JOB_NUM_NODES-1)))
do
	guix time-machine --channels=../guix-channels.scm -- \
		environment --pure --ad-hoc fxt coreutils -- \
		fxt_print -d -f $BEEGFS_PATH/${TRACE_NAME}$i | wc -l >> $DATA_PATH/cholesky_${SLURM_JOB_NUM_NODES}_no_task_deps_codelet_progress_number_events.out
done

date


for i in $(seq 0 $((SLURM_JOB_NUM_NODES-1)))
do
	mv $BEEGFS_PATH/${TRACE_NAME}$i $BEEGFS_PATH/$DATA_PATH/${SLURM_JOB_NUM_NODES}_no_task_deps_codelet_progress_prof_file_$i
done


echo "*** Performances with only sched category: ***"
guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure --preserve=^SLURM -L ../my-modules/ --ad-hoc chameleon slurm@19 \
	--with-input=starpu=my-starpu-fxt --with-commit=my-starpu-fxt=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-input=openmpi=nmad --with-input=openblas=mkl --with-patch=my-starpu-fxt=./starpu_fxt_events.patch -- \
	mpirun -DSTARPU_NCPU=35 -DSTARPU_TRACE_BUFFER_SIZE=$FXT_BUFFER_SIZE -DSTARPU_FXT_TRACE=1 -DSTARPU_FXT_PREFIX=$BEEGFS_PATH/ -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_FXT_EVENTS=sched \
	$BINARY -o potrf --n $MATRIX_SIZE -H --mtxfmt=1 --niter $NB_ITER --trace --nowarmup \
	> $DATA_PATH/cholesky_${SLURM_JOB_NUM_NODES}_only_sched.out

date

for i in $(seq 0 $((SLURM_JOB_NUM_NODES-1)))
do
	guix time-machine --channels=../guix-channels.scm -- \
		environment --pure --ad-hoc fxt coreutils -- \
		fxt_print -d -f $BEEGFS_PATH/${TRACE_NAME}$i | wc -l >> $DATA_PATH/cholesky_${SLURM_JOB_NUM_NODES}_only_sched_number_events.out
done

for i in $(seq 0 $((SLURM_JOB_NUM_NODES-1)))
do
	mv $BEEGFS_PATH/${TRACE_NAME}$i $BEEGFS_PATH/$DATA_PATH/${SLURM_JOB_NUM_NODES}_only_sched_prof_file_$i
done


echo "*** Some statistics ***"


du -h $BEEGFS_PATH/$DATA_PATH/${SLURM_JOB_NUM_NODES}_*_prof_file_* > $DATA_PATH/cholesky_${SLURM_JOB_NUM_NODES}_trace_sizes.out


echo "*** Plotting ***"
cd $DATA_PATH
../../plot.sh ${SLURM_JOB_NUM_NODES}
cd ../..

date
