#!/bin/bash

# Simple precision (matrix size 24000):
cd chameleon_stesting/24000
../../plot.sh 1
../../plot.sh 2

# Double precision (matrix size 24000):
cd ../../chameleon_dtesting/24000
../../plot.sh 1
../../plot.sh 2

# Simple precision (matrix size 40000):
cd ../../chameleon_stesting/40000
../../plot.sh 1
../../plot.sh 2

# Double precision (matrix size 40000):
cd ../../chameleon_dtesting/40000
../../plot.sh 1
../../plot.sh 2
