#!/bin/bash

export MPLBACKEND="agg"

guix time-machine --channels=../../../guix-channels.scm -- \
	environment --pure --preserve=MPLBACKEND -m ../../../guix-manifest-plot.scm -- \
	python3 ../../plot.py $1
