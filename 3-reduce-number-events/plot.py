import math
from matplotlib import pyplot as plt
from matplotlib.ticker import FuncFormatter
import pandas as pd
import os
import sys

cases = [
    {
        "filename": "cholesky_{}_no_trace.out",
        "label": "Tracing support\nnot built"
    },
    {
        "filename": "cholesky_{}_disabled_trace.out",
        "label": "Disabled\ntraces"
    },
    {
        "filename": "cholesky_{}_all_traces.out",
        "label": "All events"
    },
    {
        "filename": "cholesky_{}_without_verbose.out",
        "label": "Without\nverbose\ncategories"
    },
    {
        "filename": "cholesky_{}_no_task_deps.out",
        "label": "Without\nTASK_DEPS\nevents"
    },
    {
        "filename": "cholesky_{}_no_task_deps_codelet_progress.out",
        "label": "Without 4\nevent types"
    },
    {
        "filename": "cholesky_{}_only_sched.out",
        "label": "Only SCHED\ncategory"
    }
]


def percentile(n):
    def percentile_(x):
        return x.quantile(n)
    percentile_.__name__ = 'percentile_{:2.0f}'.format(n*100)
    return percentile_

max_gflops = 0
nb_nodes = int(sys.argv[1])
max_nb_events = 0

for c in cases:
    c['filename'] = c['filename'].format(nb_nodes)
    c['df'] = pd.read_csv(c['filename'], delim_whitespace=True)
    c['stats'] = c['df'].groupby('n').agg({"gflops": ["median", percentile(0.1), percentile(0.9)]})
    max_gflops = max(max_gflops, c['stats']['gflops']['percentile_90'].values[0])

    nb_events_filename = c['filename'].replace(".out", "_number_events.out")
    if os.path.exists(nb_events_filename):
        with open(nb_events_filename, 'r') as f:
            c['nb_events'] = sum([int(l.strip()) for l in f.readlines()])
            max_nb_events = max(max_nb_events, c['nb_events'])

cases[2:] = sorted(cases[2:], key=lambda e: e['nb_events'], reverse=True)

nb_events_x = []
nb_events_y = []

for i in range(2, len(cases)):
    nb_events_x.append(i)
    nb_events_y.append(cases[i]['nb_events'])


fig, ax = plt.subplots()

ax.grid(axis='y')
ax.set_axisbelow(True)

plt.bar(
        [c['label'] for c in cases],
        [c['stats']['gflops']['median'].values[0] for c in cases],
        yerr=[
            [c['stats']['gflops']['median'].values[0] - c['stats']['gflops']['percentile_10'].values[0] for c in cases],
            [c['stats']['gflops']['percentile_90'].values[0] - c['stats']['gflops']['median'].values[0] for c in cases]
        ],
        label="Cholesky")
ax.set(xlabel="Configurations", ylabel="Gflops")
if nb_nodes > 1:
    ax.set(title="Cholesky performance on {} nodes".format(nb_nodes))
else:
    ax.set(title="Cholesky performance on 1 node")
ax.set_ylim(top=max_gflops*1.2)
ax.legend(loc="upper left")


max_nb_events = max(nb_events_y)
max_nb_events_nb_digits = math.floor(math.log(max_nb_events, 10)) - 1

ax_right = ax.twinx()
ax_right.set_ylabel(r"Number of events x$10^{}$".format(max_nb_events_nb_digits))
ax_right.plot(nb_events_x, nb_events_y, "ro", label="Number of events")
ax_right.legend(loc="upper right")
ax_right.set_ylim(bottom=0, top=max_nb_events*1.2)
ax_right.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: "{:.1f}".format(x / 10**max_nb_events_nb_digits)))

plt.subplots_adjust(bottom=0.20)
plt.gcf().set_size_inches(plt.rcParams["figure.figsize"][0] * 1.5, plt.rcParams["figure.figsize"][1])
plt.savefig("graph_{}.pdf".format(nb_nodes))
