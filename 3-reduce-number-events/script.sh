#!/bin/bash

# Simple precision (matrix size 24000):
job_id=$(sbatch -N 1 -t 40:00 ./script.slurm.sh chameleon_stesting 24000 2048 | cut -d " " -f 4)
job_id=$(sbatch -N 2 -t 40:00 --dependency=afterany:$job_id ./script.slurm.sh chameleon_stesting 24000 2048 | cut -d " " -f 4)

# Double precision (matrix size 24000):
job_id=$(sbatch -N 1 -t 40:00 --dependency=afterany:$job_id ./script.slurm.sh chameleon_dtesting 24000 2048 | cut -d " " -f 4)
job_id=$(sbatch -N 2 -t 40:00 --dependency=afterany:$job_id ./script.slurm.sh chameleon_dtesting 24000 2048 | cut -d " " -f 4)

# Simple precision (matrix size 40000):
job_id=$(sbatch -N 1 -t 1:30:00 --dependency=afterany:$job_id ./script.slurm.sh chameleon_stesting 40000 8192 | cut -d " " -f 4)
job_id=$(sbatch -N 2 -t 1:30:00 --dependency=afterany:$job_id ./script.slurm.sh chameleon_stesting 40000 8192 | cut -d " " -f 4)

# Double precision (matrix size 40000):
job_id=$(sbatch -N 1 -t 1:30:00 --dependency=afterany:$job_id ./script.slurm.sh chameleon_dtesting 40000 8192 | cut -d " " -f 4)
job_id=$(sbatch -N 2 -t 1:30:00 --dependency=afterany:$job_id ./script.slurm.sh chameleon_dtesting 40000 8192 | cut -d " " -f 4)
