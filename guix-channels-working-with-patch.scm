;; This version of Guix is a little bit ahead of the one provided in
;; guix-channels.scm, but this one supports the combination of parameters
;; --with-commit and --with-patch, required in some experiments.
(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (commit
          "973842acbc2d0dc1ab41738a534d4abda6d9efa7")
        (introduction
          (make-channel-introduction
            "9edb3f66fd807b096b48283debdcddccfea34bad"
            (openpgp-fingerprint
              "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
      (channel
        (name 'guix-hpc-non-free)
        (url "https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free.git")
        (commit
          "157135b238595429be85f488f48c78c632c0dea7"))
      (channel
        (name 'guix-hpc)
        (url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git")
        (commit
          "fa8cc70bc428c08a625a533f778537314be64ce7")))
