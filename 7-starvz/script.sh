#!/bin/bash

if [ ! -e prof_file_pswartva_0 ]
then
	if [ ! -e ../2-number-events/simple_not_paused/prof_file_pswartva_0 ]
	then
		echo "We need the prof_file_pswartva_0 file !"
		exit 1
	fi

	ln -s ../number_events/simple_not_paused/prof_file_pswartva_0 .
fi

guix time-machine --channels=./guix-channels.scm -- \
	environment -L ../my-modules/ --pure --ad-hoc  r my-r-starvz \
	--with-commit=my-starpu-fxt=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-input=openmpi=nmad bash -- \
	bash ./starvz.sh
