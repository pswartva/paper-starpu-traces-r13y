from matplotlib import pyplot as plt
import re


files = [{
    "filename": "4_one_mpi_barrier_comms.rec",
    "label": "One MPI_Barrier"
    }, {
    "filename": "4_one_sync_comms.rec",
    "label": "One synchronized barrier"
    }, {
    "filename": "4_two_sync_comms.rec",
    "label": "Two synchronized barriers"
    }]


def convert_rec_file(filename):
    """
    Function to convert rec file to list of dict.
    """
    lines = []
    item = dict()

    with open(filename, "r") as f:
        for l in f.readlines():
            if l == "\n":
                lines.append(item)
                item = dict()
            else:
                ls = l.split(":")
                key = ls[0].lower()
                value = ls[1].strip()

                if key in item:
                    print("Warning: duplicated key '" + key + "'")
                else:
                    if re.match('^\d+$', value) != None:
                        item[key] = int(value)
                    elif re.match("^\d+\.\d+$", value) != None:
                        item[key] = float(value)
                    else:
                        item[key] = value

    return lines


fig, ax = plt.subplots()

# Times with only one MPI_Barrier are too far to show the interesting things
# about the two other files, so ignore it:
for f in files[1:]:
    data = convert_rec_file(f['filename'])
    x = []
    y = []

    for i in range(len(data)):
        x.append(((i // 4) * 100) / 1000)
        y.append((data[i]['recvtime'] - data[i]['sendtime']) * 1000)

    plt.plot(x, y, '.', label=f['label'])

y_lim_bottom, _ = ax.get_ylim()
ax.axhspan(0, y_lim_bottom-1, facecolor="0.9", zorder=0.1)
ax.set_ylim(bottom=y_lim_bottom)
ax.legend()
ax.grid()
plt.xlabel("Time progression (s)")
plt.ylabel("Communication duration (µs)")
plt.title("Communication duration according to clock synchronization method")
plt.savefig("comm_durations.pdf")
