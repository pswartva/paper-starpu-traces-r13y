#!/bin/bash

#SBATCH --tasks-per-node=1
#SBATCH --exclusive
#SBATCH -t 10:00

BEEGFS_PATH=/beegfs/pswartva/traces
TRACE_NAME=prof_file_pswartva_

export STARPU_NCPU=$((SLURM_CPUS_PER_TASK-1))
export STARPU_FXT_TRACE=1
export STARPU_FXT_PREFIX=$BEEGFS_PATH/
export STARPU_MAIN_THREAD_BIND=1

FOLDER=$1


mkdir $FOLDER

echo "*** Only one MPI_Barrier ***"
date

guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure --preserve=^SLURM --preserve=^STARPU -L ../my-modules/ --ad-hoc slurm@19 my-starpu-fxt-mpi-sync-clocks \
	--with-commit=my-starpu-fxt-mpi-sync-clocks=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-patch=my-starpu-fxt-mpi-sync-clocks=./sync_clocks_on_acae6e78df.patch --with-commit=mpi_sync_clocks=3d6d9c111 --with-input=openmpi=nmad --with-patch=my-starpu-fxt-mpi-sync-clocks=./drop_end_barrier.patch -- \
	mpirun -DSTARPU_MPI_TRACE_SYNC_CLOCKS=0 \
	./ring-dropped-end-barrier -n 20 -S 50
date

guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure -L ../my-modules/ --ad-hoc my-starpu-fxt-mpi-sync-clocks \
	--with-commit=my-starpu-fxt-mpi-sync-clocks=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-patch=my-starpu-fxt-mpi-sync-clocks=./sync_clocks_on_acae6e78df.patch --with-commit=mpi_sync_clocks=3d6d9c111 --with-input=openmpi=nmad --with-patch=my-starpu-fxt-mpi-sync-clocks=./drop_end_barrier.patch -- \
	starpu_fxt_tool -i $BEEGFS_PATH/${TRACE_NAME}*
date

rm -f activity.data dag.dot data.rec distrib.data sched_tasks.rec tasks.rec trace.html trace.rec
mv paje.trace ${FOLDER}/${SLURM_JOB_NUM_NODES}_one_mpi_barrier_paje.trace
mv comms.rec ${FOLDER}/${SLURM_JOB_NUM_NODES}_one_mpi_barrier_comms.rec

for i in $(seq 0 $((SLURM_JOB_NUM_NODES-1)))
do
	mv $BEEGFS_PATH/${TRACE_NAME}$i ${FOLDER}/${SLURM_JOB_NUM_NODES}_one_mpi_barrier_$i
done

date




echo "*** Only one precise synchronized barrier ***"
date

guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure --preserve=^SLURM --preserve=^STARPU -L ../my-modules/ --ad-hoc slurm@19 my-starpu-fxt-mpi-sync-clocks \
	--with-commit=my-starpu-fxt-mpi-sync-clocks=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-patch=my-starpu-fxt-mpi-sync-clocks=./sync_clocks_on_acae6e78df.patch --with-commit=mpi_sync_clocks=3d6d9c111 --with-input=openmpi=nmad --with-patch=my-starpu-fxt-mpi-sync-clocks=./drop_end_barrier.patch -- \
	mpirun -DSTARPU_MPI_TRACE_SYNC_CLOCKS=1 \
	./ring-dropped-end-barrier -n 100 -S 100
date

guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure -L ../my-modules/ --ad-hoc my-starpu-fxt-mpi-sync-clocks \
	--with-commit=my-starpu-fxt-mpi-sync-clocks=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-patch=my-starpu-fxt-mpi-sync-clocks=./sync_clocks_on_acae6e78df.patch --with-commit=mpi_sync_clocks=3d6d9c111 --with-input=openmpi=nmad --with-patch=my-starpu-fxt-mpi-sync-clocks=./drop_end_barrier.patch -- \
	starpu_fxt_tool -i $BEEGFS_PATH/${TRACE_NAME}* 
date

rm -f activity.data dag.dot data.rec distrib.data sched_tasks.rec tasks.rec trace.html trace.rec
mv paje.trace ${FOLDER}/${SLURM_JOB_NUM_NODES}_one_sync.trace
mv comms.rec ${FOLDER}/${SLURM_JOB_NUM_NODES}_one_sync_comms.rec

for i in $(seq 0 $((SLURM_JOB_NUM_NODES-1)))
do
	mv $BEEGFS_PATH/${TRACE_NAME}$i ${FOLDER}/${SLURM_JOB_NUM_NODES}_one_sync_$i
done

date



echo "*** Two precise synchronized barriers ***"
date

guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure --preserve=^SLURM --preserve=^STARPU -L ../my-modules/ --ad-hoc slurm@19 my-starpu-fxt-mpi-sync-clocks \
	--with-commit=my-starpu-fxt-mpi-sync-clocks=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-patch=my-starpu-fxt-mpi-sync-clocks=./sync_clocks_on_acae6e78df.patch --with-commit=mpi_sync_clocks=3d6d9c111 --with-input=openmpi=nmad -- \
	mpirun -DSTARPU_MPI_TRACE_SYNC_CLOCKS=1 \
	./ring -n 100 -S 100
date

guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure -L ../my-modules/ --ad-hoc my-starpu-fxt-mpi-sync-clocks \
	--with-commit=my-starpu-fxt-mpi-sync-clocks=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-patch=my-starpu-fxt-mpi-sync-clocks=./sync_clocks_on_acae6e78df.patch --with-commit=mpi_sync_clocks=3d6d9c111 --with-input=openmpi=nmad  -- \
	starpu_fxt_tool -i $BEEGFS_PATH/${TRACE_NAME}* 
date

rm -f activity.data dag.dot data.rec distrib.data sched_tasks.rec tasks.rec trace.html trace.rec
mv paje.trace ${FOLDER}/${SLURM_JOB_NUM_NODES}_two_sync.trace
mv comms.rec ${FOLDER}/${SLURM_JOB_NUM_NODES}_two_sync_comms.rec

for i in $(seq 0 $((SLURM_JOB_NUM_NODES-1)))
do
	mv $BEEGFS_PATH/${TRACE_NAME}$i ${FOLDER}/${SLURM_JOB_NUM_NODES}_two_sync_$i
done

date
