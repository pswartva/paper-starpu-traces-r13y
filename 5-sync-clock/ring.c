/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2009-2021  Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

#include <starpu_mpi.h>
#include <unistd.h>

#define DEFAULT_NITER 10

#define DEFAULT_DATA_SIZE 1
#define DEFAULT_INNER_RING_SLEEP_TIME 0
#define DEFAULT_OUTER_RING_SLEEP_TIME 0

static void usage()
{
	fprintf(stderr, "-n [number of iteration] (default: %d)\n", DEFAULT_NITER);
	fprintf(stderr, "-f [number of floats to exchange] (default: %d)\n", DEFAULT_DATA_SIZE);
	fprintf(stderr, "-s [time in millisecond of sleep before sending to neighbor] (default: %d)\n", DEFAULT_INNER_RING_SLEEP_TIME);
	fprintf(stderr, "-S [time in millisecond of sleep between two rings] (default: %d)\n", DEFAULT_OUTER_RING_SLEEP_TIME);
	fprintf(stderr, "-h --help display this help\n");
}


int main(int argc, char **argv)
{
	int ret, rank, worldsize, mpi_init, i;
	float *buffer_send, *buffer_recv;
	starpu_data_handle_t handle_send, handle_recv;
	double start_time, end_time;

	int niter = DEFAULT_NITER;
	int data_size = DEFAULT_DATA_SIZE;
	int outer_ring_sleep_time = DEFAULT_OUTER_RING_SLEEP_TIME;
	int inner_ring_sleep_time = DEFAULT_INNER_RING_SLEEP_TIME;

	for (i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-n") == 0)
		{
			niter = atoi(argv[i+1]);
			if (niter <= 0)
			{
				fprintf(stderr, "%s: illegal argument %s\n", argv[0], argv[i]);
				usage();
				exit(0);
			}
			i++;
		}
		else if (strcmp(argv[i], "-f") == 0)
		{
			data_size = atoi(argv[i+1]);
			if (data_size <= 0)
			{
				fprintf(stderr, "%s: illegal argument %s\n", argv[0], argv[i]);
				usage();
				exit(0);
			}
			i++;
		}
		else if(strcmp(argv[i], "-S") == 0)
		{
			outer_ring_sleep_time = atoi(argv[i+1]);
			if (outer_ring_sleep_time <= 0)
			{
				fprintf(stderr, "%s: illegal argument %s\n", argv[0], argv[i]);
				usage();
				exit(0);
			}
			i++;
		}
		else if(strcmp(argv[i], "-s") == 0)
		{
			inner_ring_sleep_time = atoi(argv[i+1]);
			if (inner_ring_sleep_time <= 0)
			{
				fprintf(stderr, "%s: illegal argument %s\n", argv[0], argv[i]);
				usage();
				exit(0);
			}
			i++;
		}
		else if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help"))
		{
			usage();
			exit(0);
		}
		else
		{
			fprintf(stderr, "%s: illegal argument %s\n", argv[0], argv[i]);
			usage();
			exit(0);
		}
	}

	ret = starpu_mpi_init_conf(&argc, &argv, 1, MPI_COMM_WORLD, NULL);
	STARPU_CHECK_RETURN_VALUE(ret, "starpu_mpi_init_conf");

	starpu_mpi_comm_rank(MPI_COMM_WORLD, &rank);
	starpu_mpi_comm_size(MPI_COMM_WORLD, &worldsize);

	if (worldsize < 2)
	{
		fprintf(stderr, "We need at least 2 processes.\n");

		starpu_mpi_shutdown();
		if (!mpi_init)
			MPI_Finalize();

		return EXIT_FAILURE;
	}

	if (rank == 0)
	{
		fprintf(stdout, "Number of MPI processes: %d\n", worldsize);
		fprintf(stdout, "Number of CPU workers: %d\n", starpu_cpu_worker_get_count());
		fprintf(stdout, "Number of iterations: %d\n", niter);
		fprintf(stdout, "Number of floats to exchange: %d (%lu B)\n", data_size, data_size*sizeof(float));
		fprintf(stdout, "Outer ring sleep time: %d milliseconds\n", outer_ring_sleep_time);
		fprintf(stdout, "Inner ring sleep time: %d milliseconds\n", inner_ring_sleep_time);
	}

	buffer_send = malloc(data_size * sizeof(float));
	buffer_recv = malloc(data_size * sizeof(float));

	starpu_vector_data_register(&handle_send, STARPU_MAIN_RAM, (uintptr_t)buffer_send, data_size, sizeof(float));
	starpu_vector_data_register(&handle_recv, STARPU_MAIN_RAM, (uintptr_t)buffer_recv, data_size, sizeof(float));

	int loop;
	int neighbor_send = (rank + 1) % worldsize; // send to the right
	int neighbor_recv = (rank + worldsize - 1) % worldsize; // receive from the left

	starpu_mpi_barrier(MPI_COMM_WORLD);
	start_time = starpu_timing_now();

	for (loop = 0; loop < niter; loop++)
	{
		if (rank > 0) // rank 0 initiates the ring
		{
			starpu_mpi_recv(handle_recv, neighbor_recv, loop, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		if (rank == 0)
		{
			if (loop > 0)
			{
				starpu_sleep(outer_ring_sleep_time / 1000.); // starpu_sleep takes seconds as float, while outer_ring_sleep_time is in ms
			}
		}
		else
		{
			starpu_sleep(inner_ring_sleep_time / 1000.);
		}
		starpu_mpi_send(handle_send, neighbor_send, loop, MPI_COMM_WORLD);
		if (rank == 0) // rank 0 ends the ring by receiving from worldsize-1
		{
			starpu_mpi_recv(handle_recv, neighbor_recv, loop, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
	}

	starpu_mpi_barrier(MPI_COMM_WORLD);
	end_time = starpu_timing_now();

	starpu_data_unregister(handle_send);
	starpu_data_unregister(handle_recv);
	free(buffer_send);
	free(buffer_recv);

	if (rank == 0)
	{
		printf("The whole execution took %.1lf seconds\n", (end_time-start_time)/1e6);
	}

	starpu_mpi_shutdown();

	return EXIT_SUCCESS;
}
