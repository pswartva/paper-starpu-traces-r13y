#!/bin/bash

guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure -L ../my-modules/ --ad-hoc my-starpu-fxt-mpi-sync-clocks gcc-toolchain pkg-config \
	--with-commit=my-starpu-fxt-mpi-sync-clocks=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-patch=my-starpu-fxt-mpi-sync-clocks=./sync_clocks_on_acae6e78df.patch --with-input=openmpi=nmad --with-commit=mpi_sync_clocks=3d6d9c111 -- \
	./build.sh ring

guix time-machine --channels=../guix-channels-working-with-patch.scm -- \
	environment --pure -L ../my-modules/ --ad-hoc my-starpu-fxt-mpi-sync-clocks gcc-toolchain pkg-config \
	--with-commit=my-starpu-fxt-mpi-sync-clocks=acae6e78df7a9475bbfbd26e33fe324b1f7bedce --with-patch=my-starpu-fxt-mpi-sync-clocks=./sync_clocks_on_acae6e78df.patch --with-input=openmpi=nmad --with-commit=mpi_sync_clocks=3d6d9c111 --with-patch=my-starpu-fxt-mpi-sync-clocks=./drop_end_barrier.patch -- \
	./build.sh ring-dropped-end-barrier

job_id=$(sbatch -C zonda --cpus-per-task=64 -N 4 ./script.slurm.sh zonda | cut -d " " -f 4)
job_id=$(sbatch -C bora --cpus-per-task=36 -N 4 --dependency=afterany:$job_id ./script.slurm.sh bora | cut -d " " -f 4)
