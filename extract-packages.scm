; Script to extract the informations displayed in the table of the README.
; It has to be executed with the following command:
; guix time-machine --channels=./guix-channels-working-with-patch.scm -- repl extract-packages.scm
(use-modules (guix packages)
             (gnu packages)
             (guix git-download))

(define packages-list '("nmad" "starpu" "chameleon" "r-starvz" "gcc-toolchain" "slurm@19" "mkl" "fxt" "python" "python-matplotlib" "python-pandas" "vite"))
(define packages-list-spec (map (lambda (p) (specification->package p)) packages-list))


(for-each (lambda (p)
       (let ((uri (and=> (package-source p) origin-uri)))
         (if (git-reference? uri)
             (format #t "~a\t~a\t~a\t~a\t~a\n"
                     (package-name p)
                     (package-version p)
                     (package-home-page p)
                     (git-reference-url uri)
                     (git-reference-commit uri))
             (format #t "~a\t~a\t~a\t~a\n"
                     (package-name p)
                     (package-version p)
                     (package-home-page p)
                     uri))))
     packages-list-spec)
