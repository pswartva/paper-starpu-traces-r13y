#!/bin/bash

#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=36
#SBATCH --exclusive
#SBATCH -C bora
#SBATCH -t 90:00

BEEGFS_PATH=/beegfs/pswartva/traces
TRACE_NAME=prof_file_pswartva_0

for nworkers in $(seq 2 35)
do
	echo "***** $nworkers workers ******"
	date

	# Without tracing:
	guix time-machine --channels=../guix-channels.scm -- \
		environment --pure --preserve=^SLURM --ad-hoc chameleon slurm@19 --with-input=openmpi=nmad --with-input=openblas=mkl -- \
		mpirun -DSTARPU_NCPU=$nworkers -DSTARPU_MAIN_THREAD_BIND=1 \
		chameleon_stesting -o potrf --n 24000 -H --mtxfmt=1 --niter 10 --nowarmup \
		> cholesky_${nworkers}.out

	date

	# With tracing:
	guix time-machine --channels=../guix-channels.scm -- \
		environment --pure --preserve=^SLURM --ad-hoc chameleon slurm@19 --with-input=starpu=starpu-fxt --with-input=openmpi=nmad --with-input=openblas=mkl -- \
		mpirun -DSTARPU_NCPU=$nworkers -DSTARPU_TRACE_BUFFER_SIZE=2048 -DSTARPU_FXT_PREFIX=$BEEGFS_PATH/ -DSTARPU_MAIN_THREAD_BIND=1 \
		chameleon_stesting -o potrf --n 24000 -H --mtxfmt=1 --niter 10 --trace --nowarmup \
		> cholesky_trace_${nworkers}.out
	ls -lh $BEEGFS_PATH/$TRACE_NAME
	rm $BEEGFS_PATH/$TRACE_NAME
done

date

./plot.sh
