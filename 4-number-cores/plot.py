import glob
import pandas as pd
from matplotlib import pyplot as plt
import re


def percentile(n):
    def percentile_(x):
        return x.quantile(n)
    percentile_.__name__ = 'percentile_{:2.0f}'.format(n*100)
    return percentile_


with_traces = dict()
without_traces = dict()

for f in glob.glob("cholesky_*.out"):
    ncores = int(f.split("_")[-1].split(".")[0])
    df = pd.read_csv(
        f,
        delim_whitespace=True
    ).groupby('n').agg({"gflops": ["median", percentile(0.1), percentile(0.9)]})

    if "trace" in f:
        assert(ncores not in with_traces)
        with_traces[ncores] = df
    else:
        assert(ncores not in without_traces)
        without_traces[ncores] = df


ax1 = plt.subplot(211)

plt.plot(
    [x for x in sorted(without_traces)],
    [without_traces[x]['gflops']['median'].values[0] for x in sorted(without_traces)],
    '.-', label="Without traces"
)
plt.fill_between(
    [x for x in sorted(without_traces)],
    [without_traces[x]['gflops']['percentile_10'].values[0] for x in sorted(without_traces)],
    [without_traces[x]['gflops']['percentile_90'].values[0] for x in sorted(without_traces)],
    alpha=0.3
)

curve_with_traces = plt.plot(
    [x for x in sorted(with_traces)],
    [with_traces[x]['gflops']['median'].values[0] for x in sorted(with_traces)],
    '.-', label="With traces"
)
plt.fill_between(
    [x for x in sorted(with_traces)],
    [with_traces[x]['gflops']['percentile_10'].values[0] for x in sorted(with_traces)],
    [with_traces[x]['gflops']['percentile_90'].values[0] for x in sorted(with_traces)],
    alpha=0.3
)

plt.setp(ax1.get_xticklabels(), visible=False)
plt.grid()
plt.ylabel("Gflops")
plt.title("Cholesky performance")
plt.ylim(bottom=0)
plt.legend()

ax2 = plt.subplot(212, sharex=ax1)

plt.plot(
    [x for x in sorted(without_traces)],
    [with_traces[x]['gflops']['median'].values[0] / without_traces[x]['gflops']['median'].values[0] for x in sorted(without_traces)],
    '.-', label="With traces", color=curve_with_traces[0].get_color()
)
ax2.grid()
ax2.set(xlabel="Number of cores", title="Slowdown relative to performance without traces", ylabel="Slowdown")
ax2.legend(loc="lower left")
ax2.set_ylim(bottom=0, top=1.1)
plt.suptitle("Impact of the number of cores on traces")
plt.gcf().set_size_inches(5.3, 5.5)
plt.savefig("graph.pdf")
